use futures_util::TryStreamExt;
use mongodb::bson::Document;
use mongodb::options::FindOptions;
use mongodb::{Client, Collection, Database};
use serde::de::DeserializeOwned;

use crate::SETTINGS;

pub trait MyDBModel {
    fn type_name() -> &'static str;
}

pub async fn establish_connection() -> Option<Database> {
    let settings = SETTINGS.get().unwrap();
    let client = Client::with_uri_str(&settings.database.url).await;
    if client.is_err() {
        return None;
    }
    Some(client.unwrap().database(&settings.database.name))
}

pub async fn get_collection<ENTITY: MyDBModel>() -> Collection<ENTITY> {
    let db = establish_connection().await.unwrap();
    db.collection::<ENTITY>(ENTITY::type_name())
}

pub async fn get_all_entries<ENTITY>(
    filter: impl Into<Option<Document>>,
    options: impl Into<Option<FindOptions>>,
) -> actix_web::Result<Vec<ENTITY>>
where
    ENTITY: MyDBModel + DeserializeOwned + Unpin + Send + Sync,
{
    let cursor = get_collection::<ENTITY>()
        .await
        .clone_with_type()
        .find(filter, options)
        .await
        .map_err(|e| actix_web::error::ErrorInternalServerError(e))?;

    cursor
        .try_collect::<Vec<ENTITY>>()
        .await
        .map_err(|e| actix_web::error::ErrorInternalServerError(e))
}
