[![Rust](https://github.com/Thunderklaud/thunder-server/actions/workflows/rust.yml/badge.svg)](https://github.com/Thunderklaud/thunder-server/actions/workflows/rust.yml)
[![Build and Publish](https://github.com/Thunderklaud/thunder-server/actions/workflows/build-and-publish.yml/badge.svg)](https://github.com/Thunderklaud/thunder-server/actions/workflows/build-and-publish.yml)

# ThunderServer
A Thunderklaud Rust Backend

Take a look at our [wiki](wiki), containing the API documentation and more administrative information.

## Features
- Store files in custom directory structures
  - supports multi file upload (using the multiform standard)
- Sharing of files and directories
  - supports date and download count limitations
- Extended download api
  - available archive download types: tar, tar.gz, zip
  - directory download packed as archive
  - direct file download (optional archive download)
- [SyncState](wiki/SyncState) API to get clients synced
- Configuration file and environment server configuration
